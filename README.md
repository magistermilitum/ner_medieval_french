### Bidirectional Long Short-Term Memory model to named entities recognition

1) This a Bi-LSTM model working at character-level with and additional CRF-Layer to train a named entities model to Middle French charters.

### Requeriments
* Tensorflow 1.15.0
* Seqeval 1.2.2
* Keras 2.3.1
* CuDA 10.1
* CuDNN 7.0
* joblib==0.14.1


### Spacy and Flair environments

1) Taggers for named entity recognition can be also trained using the annotated data in the Spacy and Flair environments. The wget to the test, val and train datasets are already indicated in the notebook, you must follow the instructions in order to fit the data into the NER architectures. We provide two Colab notebooks ready to produce robust NER models for medieval french.

2) Try the Flair Architecture: [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1DuX8xU0VQIFv8w7j_QDR5MgyznwI4-aI?usp=sharing)

3) Try the Spacy Architecture: [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1DrMRPBw8N4atJ5dhzVGYU-6QikIxajYC?usp=sharing)



### Training The custom model

1) In order to generate your custom model you must activate your Keras engine using Tensorflow as backend. If you want use your GPU you must install CUDA and CuDNNN compatible versions. See more details in [here](https://www.tensorflow.org/install/source_windows). 

2) You must load the training corpora and the architecture. Pre-loaded architecture is working using two fully connected embedding layers and TimeDistribution layer. The CRF-layer is added to exploit multidimensional features from the neural hidden-states output. 

3) The original annotated corpora is formed from Belgian and French charter collections (Diplomata Belgica and HOME)

4) In order to use your model you must charge your previous architecture and your word and character dictionnaries. An evaluation environment is pre-loaded in the jupyter workbook. 

5) The architecture can also be used to train other NER languages models as long as you load specific corpora and embeddings.


